function [p,Q]=steff(f,df,p0,delta,epsilon,max1)
  
R=zeros(max1,3);
R(1,1)=p0;

for k=1:max1
  for j=2:3

    nrdenom=df(R(k,j-1));

    if nrdenom==0
      'division by zero'
       break    
    else
      R(k,j)=R(k,j-1)-f(R(k,j-1))/nrdenom;
    end

    aadenom=R(k,3)-2*R(k,2)+R(k,1);

    if aadenom==0
      'division by zero'
      break   
    else
      R(k+1,1)=R(k,1)-(R(k,2)-R(k,1))^2/aadenom;
    end
   end

   if (nrdenom==0)||(aadenom==0)
     break
   end

    err=abs(R(k,1)-R(k+1,1));
    relerr=err/(abs(R(k+1,1))+delta);
    y=f(R(k+1,1));
    if (err<delta)||(relerr<delta)||(y<epsilon)
       p=R(k+1,1);
       Q=R(1:k+1,:);
       break
    end
end

% >> [p,Q]=steff(@nf,@df,0,0.001,0.00000000001,100)
% p = 0.8177
% Q =
%
%        0   1.0000   0.8333
%   0.8571   0.8185   0.8177
%   0.8177        0        0

function [c,err,yc]=regula(f,a,b,delta,epsilon,max1)

ya=f(a);
yb=f(b);

if ya*yb>0
  'not different signs'
	return,
end

for k=1:max1
	dx=yb*(b-a)/(yb-ya);
	c=b-dx;
	ac=c-a;
	yc=f(c);
	if yc==0,break;
	elseif yb*yc>0
		b=c;
		yb=yc;
	else
		a=c;
		ya=yc;
	end
	dx=min(abs(dx),ac);
	if abs(dx)<delta,break,end
	if abs(yc)<epsilon, break,end
end

c;
err=abs(b-a)/2;
yc=f(c);

% call [c,err,yc]=regula(@f,0,2,0.001,0.0000000001,100)
% c = 1.1142
% err = 7.2035e-03
% yc = 3.0023e-09

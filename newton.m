function [p0,err,k,y]=newton(f,df,p0,delta,epsilon,max1)

for k=1:max1  
  p1=p0-f(p0)/df(p0);  
  err=abs(p1-p0);
  relerr=2*err/(abs(p1)+delta);
  p0=p1;
  y=f(p0);
  if (err<delta)||(relerr<delta)||(abs(y)<epsilon),break,end
end

% >> [p0,err,k,y]=newton(@nf,@df,0,0.001,0.0000000001,100)
% p0 = 0.8177
% err = 1.1896e-04
% k = 4
% y = 3.4718e-08
%
% f = x^3+3*x-3
% df = 3*x^2+3
